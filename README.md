# Challenge Follow

Este proyecto se utilizará para resolver un desafío de programación.

# Objetivos

Conocer el manejo de las tecnologías base de los participantes y observar su forma de trabajo antes dudas y/o obstáculos presentados.

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Java
Maven
MySql
Lombok
```
### Instalación 🔧

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

_Dí cómo será ese paso_

```
Instalar Java 8
```
```
Instalar Maven
```
```
Instalar MySQL e importar la bd de ejemplo. (Se adjunta)
```
```
Instalar la librería Lombok en la raiz del Ide utilizado (https://projectlombok.org/)
```
```
Descargar Repositorio (https://gitlab.com/fedeacevedo/challenge.git) en la rama challenge_endpoint
```
```
Crear una rama con el siguiente formato: 'challenge_' + tu nombre + '_' + 'tu apellido' desde la rama 'challenge_endpoint' - Ej: challenge_federico_acevedo
```
```
Levantar el proyecto con el Ide que deseen
```
```
Realizar los cambios convenientes y hacer push a la rama creada.
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Spring](https://spring.io/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias


